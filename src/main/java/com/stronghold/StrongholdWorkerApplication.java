package com.stronghold;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrongholdWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrongholdWorkerApplication.class, args);
	}
}

package com.stronghold.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Set;

@Configuration
public class HazelcastConfiguration {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Bean
    public IMap<String, String> requestMap() {
        IMap<String, String> requestMap = hazelcastInstance.getMap("requestMap");
        requestMap.put("1", "one");
        requestMap.put("2", "two");
        requestMap.put("3", "three");
        requestMap.put("4", "four");
        requestMap.put("5", "five");
        requestMap.put("6", "six");

        return requestMap;

    }

    @Bean
    public HazelcastInstance hazelcastInstance() {
        Config config = new Config();
        NetworkConfig networkConfig = config.getNetworkConfig();
        networkConfig.setPort(5081).setPortAutoIncrement(true);

        JoinConfig joinConfig = networkConfig.getJoin();
        joinConfig.getMulticastConfig().setEnabled(false);
        joinConfig.getTcpIpConfig().addMember("127.0.0.1").setEnabled(true);

        return Hazelcast.newHazelcastInstance(config);
    }

    @Bean
    public Set<Member> allMembers() {
        return hazelcastInstance.getCluster().getMembers();
    }
}

package com.stronghold.service;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@RestController
public class DistributedMapService {
    @Autowired
    private IMap<String, String> requestMap;

    @Autowired
    private Set<Member> allMembers;

    public Set<String> getLocalEntries() {
        return requestMap.localKeySet();
    }

    @RequestMapping("/nodes")
    public String getMembers() {
        return allMembers.toString();
    }
}
